package com.jbr330.employeerestapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    @CrossOrigin
    @GetMapping("/employees")
    public static ArrayList<Employee> getEmployeeList() {
        Employee employee1 = new Employee(1, "Peter", "Pan", 9000);
        Employee employee2 = new Employee(2, "Thomas", "Brand", 8000);
        Employee employee3 = new Employee(3, "Jack", "Potter", 7000);
        System.out.println("employee1 la: " + employee1);
        System.out.println("employee2 la: " + employee2);
        System.out.println("employee3 la: " + employee3);
        ArrayList<Employee> employeeList = new ArrayList<Employee>();
        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);
        return employeeList;
    }
}
